//
//  SideMenuManager.swift
//  SideMenu
//
//  Created by Ly Boung on 16/8/23.
//

import UIKit

public class SideMenuManager {
  public static let shared = SideMenuManager()
  
  public var preferredMenuSize: CGFloat = 0.8 {
    didSet {
      if preferredMenuSize < 0.1 {
        return preferredMenuSize = 0.1
      }
      
      if preferredMenuSize > 1.0 {
        return preferredMenuSize = 1.0
      }
      
      drawerVC?.preferredMenuSize = preferredMenuSize
    }
  }
  public var preferredAnimationDuraion: TimeInterval = 0.3 {
    didSet {
      drawerVC?.preferredAnimationDuraion = preferredAnimationDuraion
    }
  }
  public var preferredBackgroundColor: UIColor = .black.withAlphaComponent(0.5) {
    didSet {
      drawerVC?.preferredBackgroundColor = preferredBackgroundColor
    }
  }
  
  public var menuState: SideMenuViewController.SideMenuState {
    return drawerVC?.menuState ?? .closed
  }
  public var mainViewController: UIViewController? {
    return drawerVC?.mainViewController
  }
  public var menuViewController: UIViewController? {
    return drawerVC?.menuViewController
  }
  
  private var valueChangeHandler: ((Any?) -> Void)?
  private var drawerVC: SideMenuViewController?
  private init() { }
  
  public func initializeSideMenuController(mainVC: UIViewController, menuVC: UIViewController, tapDimissMenu: Bool = true, direction: SideMenuViewController.SideMenuDirection = .leftToRight) -> SideMenuViewController {
    let vc = SideMenuViewController(mainVC: mainVC, menuVC: menuVC, tapDismissMenu: tapDimissMenu, direction: direction)
    drawerVC = vc
    drawerVC?.preferredMenuSize = preferredMenuSize
    drawerVC?.preferredAnimationDuraion = preferredAnimationDuraion
    drawerVC?.preferredBackgroundColor = preferredBackgroundColor
    
    return vc
  }
  
  public func replaceMainViewController(to controller: UIViewController) {
    drawerVC?.replaceMainViewController(to: controller)
  }
  
  public func toggleMenuState() {
    drawerVC?.toggleMenuState()
  }
  
  public func postValueChangeHandler(_ payload: Any?) {
    valueChangeHandler?(payload)
  }
  
  public func setValueChangedObserver(handler: ((Any?) -> Void)?) {
    self.valueChangeHandler = handler
  }
}
