//
//  SideMenuViewController.swift
//  SideMenu
//
//  Created by Ly Boung on 15/8/23.
//

import UIKit

public class SideMenuViewController: UIViewController {
  public enum SideMenuState {
    case closed
    case opened
  }
  
  public enum SideMenuDirection {
    case leftToRight
    case rightToLeft
  }
  
  public var mainViewController: UIViewController
  public var menuViewController: UIViewController
  var menuState: SideMenuState = .closed
  
  var preferredMenuSize: CGFloat = 0.8
  var preferredAnimationDuraion: TimeInterval = 0.3
  var preferredBackgroundColor: UIColor = .black.withAlphaComponent(0.5)
  
  private var menuDirection: SideMenuDirection = .leftToRight
  private var tapDismissMenu: Bool = true
  private var embeddedNavigationController: Bool = false
  private var viewWidth: CGFloat {
    return view.bounds.width
  }
  private var menuSize: CGFloat {
    return viewWidth * preferredMenuSize
  }
  private lazy var backgroundView: UIView = {
    let view = UIView()
    view.backgroundColor = preferredBackgroundColor
    view.translatesAutoresizingMaskIntoConstraints = false
    
    return view
  }()
  
  init(mainVC: UIViewController, menuVC: UIViewController, tapDismissMenu: Bool = true ,direction: SideMenuDirection = .leftToRight) {
    self.mainViewController = mainVC
    self.menuViewController = menuVC
    self.menuDirection = direction
    self.tapDismissMenu = tapDismissMenu
    
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    self.mainViewController = UIViewController()
    self.menuViewController = UIViewController()
    
    super.init(coder: coder)
  }
  
  public override func viewDidLoad() {
    super.viewDidLoad()
    
    initializeMenu()
    
    addChildVCs()
    addSwipeGesture()
  }
  
  func toggleMenuState() {
    switch menuState {
      case .closed:
        menuState = .opened
      case .opened:
        menuState = .closed
    }
    
    toggleMenu()
  }
  
  func replaceMainViewController(to controller: UIViewController) {
    remvoeChildVCs()
    
    mainViewController = controller
    addChildVCs()
  }
}

extension SideMenuViewController {
  
  private func addChildVCs() {
    addChild(mainViewController)
    view.addSubview(mainViewController.view)
    mainViewController.didMove(toParent: self)
    
    addChild(menuViewController)
    view.addSubview(menuViewController.view)
    menuViewController.didMove(toParent: self)
  }
  
  private func remvoeChildVCs() {
    menuViewController.willMove(toParent: nil)
    menuViewController.view.removeFromSuperview()
    menuViewController.removeFromParent()
    
    mainViewController.willMove(toParent: nil)
    mainViewController.view.removeFromSuperview()
    mainViewController.removeFromParent()
  }
  
  private func initializeMenu() {
    menuViewController.view?.frame.size.width = menuSize
    menuViewController.view?.frame.origin.x = menuDirection == .leftToRight ? -menuSize : viewWidth
  }
  
  private func toggleMenu() {
    let view = menuViewController.view
    
    UIView.animate(withDuration: preferredAnimationDuraion, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear) { [weak self] in
      guard let self = self else { return }
      
      switch menuState {
        case .closed:
          view?.frame.origin.x = menuDirection == .leftToRight ? -menuSize : self.viewWidth
        case .opened:
          view?.frame.origin.x = menuDirection == .leftToRight ? 0 : self.viewWidth - menuSize
      }
      
      self.toggleShadowView()
    }
  }
  
  private func toggleShadowView() {
    backgroundView.isUserInteractionEnabled = tapDismissMenu
    
    if tapDismissMenu {
      let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onShadowViewTap))
      backgroundView.addGestureRecognizer(tapGesture)
    }
    
    switch menuState {
      case .opened:
        mainViewController.view.addSubview(backgroundView)
        backgroundView.topAnchor.constraint(equalTo: mainViewController.view.topAnchor, constant: 0).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: mainViewController.view.leadingAnchor, constant: 0).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: mainViewController.view.trailingAnchor, constant: 0).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: mainViewController.view.bottomAnchor, constant: 0).isActive = true
      case .closed:
        backgroundView.removeFromSuperview()
    }
  }
  
  @objc private func onShadowViewTap() {
    toggleMenuState()
  }
  
  private func addSwipeGesture() {
    let leftSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(onSwipeHandler(_:)))
    leftSwipeGesture.direction = .left
    view.addGestureRecognizer(leftSwipeGesture)
    
    let rightSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(onSwipeHandler(_:)))
    rightSwipeGesture.direction = .right
    view.addGestureRecognizer(rightSwipeGesture)
  }
  
  @objc private func onSwipeHandler(_ gesture: UISwipeGestureRecognizer) {
    switch menuDirection {
      case .leftToRight:
        switch gesture.direction {
          case .left:
            if menuState == .closed { return }
            toggleMenuState()
          case .right:
            if menuState == .opened { return }
            toggleMenuState()
          default:
            break
        }
      case .rightToLeft:
        switch gesture.direction {
          case .left:
            if menuState == .opened { return }
            toggleMenuState()
          case .right:
            if menuState == .closed { return }
            toggleMenuState()
          default:
            break
        }
    }
  }
}
