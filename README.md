<h1>SideMenuSPM</h1>
A Simple SPM for Side menu controller.

<h2>Initializing Controller</h2>
<b>Call this function to get SideMenuViewController</b> <br/>

``` 
initializeSideMenuController(mainVC: UIViewController, menuVC: UIViewController, tapDimissMenu: Bool = true, direction: SideMenuViewController.SideMenuDirection = .leftToRight) -> SideMenuViewController
```

<pre>
  <b>mainVC</b>: property to take MainViewController
  <b>menuVC</b>: property to take MenuViewController
  <b>tapDimissMenu</b>: property to take boolean value to able to tap dismiss SideMenu
  <b>direction</b>: enum property to take to set direction for menu
    <b>leftToRight</b>: SideMenu will move from Left to Right
    <b>rightToLeft</b>: SideMenu will move from Right to Left
</pre>

<h2>Properties</h2>

<b>SideMenu's size from one edge to the other edge</b> <br/>
<b>Datatype:</b> CGFloat <br/>
<b>Default Value:</b> 0.8 <br/>
<b>Value Range:</b> 0.1 -> 1.0

```
SideMenuManager.shared.preferredMenuSize
```

<b>SideMenu's animation duration</b> <br/> 
<b>Datatype:</b> TimeInterval<br/>
<b>default Value:</b> 0.3

```
SideMenuManager.shared.preferredAnimationDuraion
```

<b>SideMenu's background color when open</b> <br/>
<b>Datatype:</b> CGFloat <br/>
<b>Default Value:</b> UIColor.black.withAlphaComponent(0.5)

```
SideMenuManager.shared.preferredBackgroundColor
```

<h2>Functions</h2>
<b>Toggle SideMenu</b>

```
SideMenuManager.shared.toggleMenuState()
```

<b>Add ValueChange Listener</b> <br/>
<b>payload</b>: payload is type Any. So in use case, we can convert the payload to desired type you send from <b>postValueChangeHandler</b>

```
SideMenuManager.shared.setValueChangedObserver { payload in }
```

<b>Post ValueChanged Observer</b>

```
SideMenuManager.shared.postValueChangeHandler(payload: Any)
```

<h1>Demo Example</h1>

<b>AppDelegate.swift</b>
```
import UIKit
import SideMenuSPM

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    configRoot()
    
    return true
  }
  
  private func configRoot() {
    if #available(iOS 15, *) {
      let appearance = UINavigationBarAppearance()
      appearance.configureWithOpaqueBackground()
      appearance.backgroundColor = .orange
      UINavigationBar.appearance().standardAppearance = appearance
      UINavigationBar.appearance().scrollEdgeAppearance = appearance
    }
    
    let controller = MainViewController()
    controller.title = "Main Controller"
    let navVC = UINavigationController(rootViewController: controller)
    
    window = UIWindow()
    window?.rootViewController = SideMenuManager.shared.initializeSideMenuController(mainVC: navVC, menuVC: SideMenuViewController(), tapDimissMenu: true, direction: .leftToRight)
    window?.makeKeyAndVisible()
  }
}
```

<b>MainViewController.swift</b>
```
import UIKit
import SideMenuSPM

class MainViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .systemBlue
    
    SideMenuManager.shared.setValueChangedObserver { payload in
      // MARK: - Toggle Side Menu after action on menu side
      SideMenuManager.shared.toggleMenuState()
      
      guard let selectedIndex = payload as? Int else { return }
      // MARK: - Handle Action base on payload selectedIndex
      // TODO: - YOUR CODE HERE
    }
  }
}
```

<b>SideMenuViewController.swift</b>
```
import UIKit
import SideMenuSPM

class SideMenuViewController: UIViewController {
  
  private var tableView: UITableView = {
    let tableView = UITableView()
    tableView.backgroundColor = .white
    tableView.translatesAutoresizingMaskIntoConstraints = false
    
    return tableView
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    prepareLayouts()
  }
  
  private func prepareLayouts() {
    view.backgroundColor = .orange
    
    tableView.delegate = self
    tableView.dataSource = self
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    
    view.addSubview(tableView)
    tableView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
    tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    tableView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
  }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
    cell.textLabel?.text = "Click Me"
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    // MARK: - Post action to listen -> Sending IndexPath.row as payload for listener to handle accordingly.
    SideMenuManager.shared.postValueChangeHandler(indexPath.row)
  }
}

```

<h1> Demo Project </h1>
Please refer to this <a href="https://gitlab.com/Boung/demosidemenuspm">Demo Project</a>.
